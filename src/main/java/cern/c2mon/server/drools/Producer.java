package cern.c2mon.server.drools;

import cern.c2mon.shared.client.tag.TransferTagImpl;
import cern.c2mon.shared.common.datatag.*;
import cern.c2mon.shared.daq.datatag.DataTagValueUpdateConverter;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import javax.jms.Destination;

@Component
public class Producer {

  @Autowired
  private JmsTemplate jmsTemplate;

  public void sendRuleResult(String ruleName) {
    this.jmsTemplate.setMessageConverter(new DataTagValueUpdateConverter());
    Destination queue = new ActiveMQQueue("c2mon.rules.result");
    SourceDataTagValue sdtv = new SourceDataTagValue();
    sdtv.setId(-1L);
    sdtv.setName("rule_result");
    sdtv.setValueDescription(ruleName);
    sdtv.setValue(true);
    DataTagValueUpdate x = new DataTagValueUpdate(-1L);
    x.addValue(sdtv);
    this.jmsTemplate.convertAndSend(queue, x);
  }


}
