import cern.c2mon.shared.client.tag.TransferTagImpl;
import cern.c2mon.shared.client.alarm.AlarmValueImpl
import cern.c2mon.server.drools.Producer;

global Producer producer;

declare TransferTagImpl
    @role(event)
    @timestamp(daqTimestamp)
end

declare AlarmActive
    message: String
end

rule "temperature is updated"
    when
        tag: TransferTagImpl(tagName matches ".*/cpu.temp")
    then
        producer.sendRuleResult(drools.getRule().getName());
end

rule "temperature is above 40"
when
    tag: TransferTagImpl(name matches ".*/cpu.temp", value > 40.0)
then
    System.out.println(tag.getName() + " temperature above 40" + tag.getUnit() + " (" + tag.getValue() + tag.getUnit() + ")");
    producer.sendRuleResult(drools.getRule().getName());
end

rule "temperature outside range"
when
    tag: TransferTagImpl(name matches ".*/cpu.temp", value < 10 || value > 40)
then
    System.out.println(tag.getName() + " outside of good range");
end

rule "temperature in building 864"
    when
        tag: TransferTagImpl(name matches ".*/cpu.temp", metadata.get("location") == "864")
    then
        System.out.println("Temperature updated in location 864");
end

rule "average CPU temp over 40"
when
       $avg: Number( doubleValue > 40.0) from accumulate (
         TransferTagImpl($value: value, name matches ".*/cpu.temp") over window:time( 10m ),
         average($value)
       )
then
    System.out.println("HIGH TEMPERATURE: " + $avg + "C");
    AlarmActive $alarm = new AlarmActive();
    $alarm.setMessage("OH NO!");
    insertLogical($alarm); //retracted when rule is no longer active
end


rule "std dev of temp"
    when
        $std: Number (doubleValue > 10.0) from accumulate (
            TransferTagImpl($value: value, name matches ".*/cpu.temp", metadata.get("location") == "864") over window:time(5m),
            standardDeviation($value)
        )
    then
        System.out.println("stddev over 5 minutes is over 10%: " + $std);
end

rule "more than 1 alarm"
    when
        $n: Number (intValue >= 1) from accumulate (
            AlarmValueImpl(faultFamily == "cpu.temp"),
            count(1)
        )
    then
        System.out.println("Number of alarms is " + $n);
end

rule "alarm active"
    when
        $alarm: AlarmValueImpl()
    then
        System.out.println("OMG there's an alarm: " + $alarm);
end

rule "print message when alarm is active"
when
    $alarm: AlarmActive()
then
    System.out.println($alarm.getMessage());
end


rule "timed test"
    timer (int: 30s 30s)
then
    System.out.println("TIMER every 30 seconds");
end
